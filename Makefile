INCLUDES_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

DC=docker-compose
DCE=$(DC) exec
DJANGO_CONTAINER=django

HELP_PROGRAM := $(INCLUDES_DIR)/help.awk

#
# Aliases
#
u: up
upd: up-daemon
s: stop
m: manage

#
# Commands
#
#> (Alias=u)
#> Start docker containers
up:
	@$(DC) up

#> (Alias=upd)
#> Start docker containers in a daemon
up-daemon:
	@$(DC) up -d

#> (Alias=stop)
#> Stop docker containers
stop:
	@$(DC) stop

#> (Alias=m)
#> Run a manage.py command through the django container.
#> Usage: make manage <command> -- [flags...]
manage:
	@$(DCE) $(DJANGO_CONTAINER) python manage.py $(filter-out $@, $(MAKECMDGOALS))

dump-groups:
	@$(DCE) $(DJANGO_CONTAINER) python manage.py dumpdata auth.Group auth.Permission -o app/fixtures/data.json --indent 2

load-groups:
	@$(DCE) $(DJANGO_CONTAINER) python manage.py loaddata app/fixtures/data.json

#> (Current)
#> See all available commands
h: _HELP_F := $(firstword $(MAKEFILE_LIST))
h: | _program_awk
	@awk -f $(HELP_PROGRAM) $(wordlist 2,$(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)) $(_HELP_F)  # always prefer help from the top-level makefile
.DEFAULT_GOAL := h

# If command not found
%:
	@: # Do nothing, silently

.PHONY: manage h up up-daemon stop django-admin