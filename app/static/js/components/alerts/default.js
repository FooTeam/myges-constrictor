export class Alert extends HTMLElement {
  panel = document.createElement("div");

  constructor(icon) {
    self = super();

    const title = this.getAttribute("data-title");
    const text = this.getAttribute("data-text");

    this.attachShadow({ mode: "open" });

    const wrapper = document.createElement("div");
    wrapper.setAttribute("aria-live", "assertive");
    wrapper.setAttribute(
      "class",
      "fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start"
    );

    this.panel = document.createElement("div");
    this.panel.setAttribute(
      "class",
      "w-full flex flex-col items-center space-y-4 sm:items-end"
    );

    const card = document.createElement("div");
    card.setAttribute(
      "class",
      "flex items-start p-4 max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden"
    );

    if (icon) {
      const iconContainer = document.createElement("div");
      iconContainer.setAttribute("class", "flex-shrink-0");
      iconContainer.innerHTML = icon;
      card.appendChild(iconContainer);
    }

    const contentContainer = document.createElement("div");
    contentContainer.setAttribute("class", "ml-3 w-0 flex-1 pt-0.5");
    if (title !== null) {
      const titleNode = document.createElement("p");
      titleNode.setAttribute("class", "text-sm font-medium text-gray-900");
      titleNode.appendChild(document.createTextNode(title));
      contentContainer.appendChild(titleNode);
    }
    if (text !== null) {
      const textNode = document.createElement("p");
      textNode.setAttribute("class", "mt-1 text-sm text-gray-500");
      textNode.appendChild(document.createTextNode(text));
      contentContainer.appendChild(textNode);
    }
    card.appendChild(contentContainer);

    const actionContainer = document.createElement("div");
    actionContainer.setAttribute("class", "ml-4 flex-shrink-0 flex");
    const button = document.createElement("button");
    button.setAttribute(
      "class",
      "bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
    );
    button.addEventListener("click", () => {
      self.close();
    });

    button.innerHTML = `
        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
          <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
        </svg>
      `;

    const span = document.createElement("span");
    span.setAttribute("class", "sr-only");
    span.appendChild(document.createTextNode("Close"));
    button.appendChild(span);

    actionContainer.appendChild(button);
    card.appendChild(actionContainer);

    this.panel.appendChild(card);
    wrapper.appendChild(this.panel);

    const linkElem = document.createElement("link");
    linkElem.setAttribute("rel", "stylesheet");
    linkElem.setAttribute("href", "/static/css/dist/styles.css");

    this.shadowRoot.append(linkElem);
    this.shadowRoot.append(wrapper);
    setTimeout(() => self.close(), 10000);
  }

  close() {
    this.shadowRoot.innerHTML = "";
  }
}

customElements.define("alert-default", Alert);
