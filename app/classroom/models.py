from django.db import models
from django.contrib.auth.models import AbstractUser


class Promotion(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class User(AbstractUser):
    promotion = models.ForeignKey(
        Promotion, on_delete=models.PROTECT, null=True)


class Subject(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()

    promotions = models.ManyToManyField(Promotion)
    teacher = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.name
