from django.views.generic import UpdateView, CreateView
from .models import User
from classroom.forms import ProfileForm, StudentForm
from classroom.templatetags.classroom_extras import is_coordinator, is_teacher
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import Group
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'classroom/home.html'


class CoordinatorView(LoginRequiredMixin, UserPassesTestMixin):
    """Class to extends for views reserved to coordinators"""

    def test_func(self):
        return is_coordinator(self.request.user)


class TeacherView(LoginRequiredMixin, UserPassesTestMixin):
    """Class to extends for views reserved to coordinators"""

    def test_func(self):
        return is_teacher(self.request.user)


class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'classroom/profile.html'
    form_class = ProfileForm
    model = User
    success_url = '/profile'

    def get_object(self):
        return self.request.user


class UserProfileView(CoordinatorView, UpdateView):
    template_name = 'classroom/profile.html'
    form_class = ProfileForm
    model = User
    pk_url_kwarg = 'id'
    success_url = '/profile'



class StudentCreateView(CoordinatorView, CreateView):
    model=User
    form_class = StudentForm
    template_name = 'classroom/create_student.html'
    success_url = '/promotions'

    def test_func(self):
        return is_coordinator(self.request.user)

    def post(self, request):
        form = StudentForm(request.POST)
        if not form.is_valid():
            return render(request, StudentCreateView().template_name, {'form': form})
        
        student = User()
        student.username = form.cleaned_data['username']
        student.first_name = form.cleaned_data['first_name']
        student.last_name = form.cleaned_data['last_name']
        student.email = form.cleaned_data['email']
        student.password = "foobarbaz"
        student.save()
        
        Group.objects.get(name='students').user_set.add(student)

        return redirect('promotion.coordinator')
