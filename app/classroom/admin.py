from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Subject, Promotion


admin.site.register(User, UserAdmin)
admin.site.register(Subject)
admin.site.register(Promotion)
