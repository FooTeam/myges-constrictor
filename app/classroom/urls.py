from django.urls import include, path
from classroom.views import HomeView, ProfileView, StudentCreateView, UserProfileView
from classroom.model_views.subjects import SubjectIndex, SubjectCreateView, SubjectEditView, SubjectDeleteView
from classroom.model_views.teachers import TeacherIndex, TeacherCreateView, TeacherEditView, TeacherDeleteView
from classroom.model_views.promotions import PromotionViewStudent, PromotionViewCoordinator, PromotionViewCoordinatorDetail, PromotionViewCoordinatorAddStudent, PromotionViewCoordinatorRemoveStudent, AddPromotionView
from classroom.model_views.teachers_subjects import TeacherSubjectsIndex, TeacherSubjectMarksIndex, TeacherAddMark

subjects_urlpatterns = [
    path('', SubjectIndex.as_view(), name='subjects.index'),
    path('create', SubjectCreateView.as_view(), name='subjects.create'),
    path('<int:id>', SubjectEditView.as_view(), name='subjects.edit'),
    path('<int:id>/delete', SubjectDeleteView.as_view(), name="subjects.delete"),
]

subjects_for_teachers_urlpatterns = [
    path('', TeacherSubjectsIndex.as_view(), name='teachers.subjects.index'),
    path(
        '<int:subject_id>/manage_marks',
        TeacherSubjectMarksIndex.as_view(),
        name='teachers.subjects.manage_marks'
    ),
    path(
        '<int:subject_id>/add_mark',
        TeacherAddMark.as_view(),
        name='teachers.add_mark'
    )
]

teachers_urlpatterns = [
    path('', TeacherIndex.as_view(), name='teachers.index'),
    path('create', TeacherCreateView.as_view(), name='teachers.create'),
    path('<int:id>', TeacherEditView.as_view(), name='teachers.edit'),
    path('<int:id>/delete', TeacherDeleteView.as_view(), name="teachers.delete"),

    path('subjects/', include(subjects_for_teachers_urlpatterns)),
]

promotion_urlpatterns = [
    path('', PromotionViewCoordinator.as_view(), name="promotion.coordinator"),
    path('/<int:id>', PromotionViewCoordinatorDetail.as_view(),
         name="promotion.coordinator.detail"),
    path('/<int:id>/add-student', PromotionViewCoordinatorAddStudent.as_view(),
         name="promotion.coordinator.add.student"),
    path('/remove-student', PromotionViewCoordinatorRemoveStudent.as_view(),
         name="promotion.coordinator.remove.student"),
    path('/create', AddPromotionView.as_view(), name="promotion.create")
]

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('create-student', StudentCreateView.as_view(), name="create.student"),
    path('subjects/', include(subjects_urlpatterns)),
    path('teachers/', include(teachers_urlpatterns)),
    path('profile', ProfileView.as_view(), name='profile'),
    path('user/<int:id>/profile', UserProfileView.as_view(), name='user.profile'),
    path('promotion', PromotionViewStudent.as_view(), name="promotion.student"),
    path('promotions', include(promotion_urlpatterns)),
]
