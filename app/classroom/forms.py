from django.forms import ModelForm
from .models import Subject, User, Promotion
from mark.models import Mark


class SubjectForm(ModelForm):
    class Meta:
        model = Subject
        fields = ['name', 'description', 'promotions', 'teacher']


class TeacherForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class ProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class StudentForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class MarkForm(ModelForm):
    class Meta:
        model = Mark
        fields = ['value', 'student']

class PromotionForm(ModelForm):
    class Meta:
        model = Promotion
        fields = ['name']