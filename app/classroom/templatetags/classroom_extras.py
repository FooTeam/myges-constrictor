from django import template

register = template.Library()


def is_student(user):
    return user.groups.filter(name='students').exists()


def is_teacher(user):
    return user.groups.filter(name='teachers').exists()


def is_coordinator(user):
    return user.groups.filter(name='coordinators').exists()


register.filter('is_student', is_student)
register.filter('is_teacher', is_teacher)
register.filter('is_coordinator', is_coordinator)
