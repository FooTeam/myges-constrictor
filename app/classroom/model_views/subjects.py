from django.views.generic import UpdateView, CreateView, DeleteView, ListView
from classroom.models import Subject, User
from classroom.forms import SubjectForm
from classroom.views import CoordinatorView
from django.shortcuts import render, redirect


class SubjectIndex(CoordinatorView, ListView):
    model = Subject
    template_name = 'classroom/subject/index.html'
    context_object_name = 'subjects'


class SubjectCreateView(CoordinatorView, CreateView):
    model = Subject
    form_class = SubjectForm
    template_name = 'classroom/subject/edit.html'
    success_url = '/subjects'

    def get(self, request):
        form = SubjectForm()
        teachers = User.objects.filter(groups__name__in=["teachers"])
        return render(request, SubjectEditView.template_name, {'form': form, 'teachers': teachers})


class SubjectEditView(CoordinatorView, UpdateView):
    model = Subject
    form_class = SubjectForm
    pk_url_kwarg = 'id'
    template_name = 'classroom/subject/edit.html'
    success_url = '/subjects'


class SubjectDeleteView(CoordinatorView, DeleteView):
    model = Subject
    pk_url_kwarg = 'id'
    template_name = 'classroom/subject/delete.html'
    success_url = '/subjects'
