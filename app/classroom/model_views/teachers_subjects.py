from django.views.generic import UpdateView, CreateView, DeleteView, ListView
from django.views.generic.base import TemplateView
from classroom.models import Subject, User, Promotion
from mark.models import Mark
from django.shortcuts import render, redirect
from classroom.views import TeacherView
from django.contrib.auth.models import Group
from classroom.forms import MarkForm
from django.urls import reverse


class TeacherSubjectsIndex(TeacherView, ListView):
    model = Subject
    template_name = 'classroom/teacher_subjects/index.html'
    context_object_name = 'subjects'

    def get_queryset(self):
        return Subject.objects.filter(teacher=self.request.user)


class TeacherSubjectMarksIndex(TeacherView, TemplateView):
    template_name = 'classroom/teacher_subjects/manage_marks.html'

    def get(self, request, subject_id):
        marks = {}
        for mark in Mark.objects.filter(subject__id=subject_id):
            promotion = 'Étudiant libre' if mark.student.promotion is None else mark.student.promotion.name
            marks[promotion] = [
                mark] if promotion not in marks else marks[promotion] + [mark]

        return render(request, TeacherSubjectMarksIndex.template_name, {'marks': marks, 'subject': Subject.objects.get(pk=subject_id)})


class TeacherAddMark(TeacherView, TemplateView):
    template_name = 'classroom/teacher_subjects/add_mark.html'

    def get(self, request, subject_id):
        form = MarkForm()
        subject = Subject.objects.get(pk=subject_id)
        students = User.objects.filter(
            promotion__in=Promotion.objects.filter(subject__in=[subject])
        )
        return render(request, TeacherAddMark.template_name, {'form': form, 'subject': subject, 'students': students})

    def post(self, request, subject_id):
        form = MarkForm(request.POST)
        subject = Subject.objects.get(pk=subject_id)

        if not form.is_valid():
            return render(
                request,
                TeacherAddMark.template_name,
                {'form': form, 'subject': subject}
            )

        mark = Mark()
        mark.value = form.cleaned_data['value']
        mark.student = form.cleaned_data['student']
        mark.subject = subject
        mark.save()

        return redirect(reverse('teachers.subjects.manage_marks', args=[subject.id]))
