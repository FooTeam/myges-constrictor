from django.views.generic import UpdateView, CreateView, DeleteView, ListView
from classroom.models import User
from classroom.forms import TeacherForm
from django.shortcuts import render, redirect
from classroom.views import CoordinatorView
from django.contrib.auth.models import Group


class TeacherIndex(CoordinatorView, ListView):
    model = User
    template_name = 'classroom/teacher/index.html'
    context_object_name = 'teachers'
    queryset = User.objects.filter(groups__name__in=['teachers', ])


class TeacherCreateView(CoordinatorView, CreateView):
    model = User
    form_class = TeacherForm
    template_name = 'classroom/teacher/edit.html'
    success_url = '/subjects'

    def post(self, request):
        form = TeacherForm(request.POST)

        if not form.is_valid():
            return render(
                request,
                TeacherCreateView.template_name,
                {'form': form}
            )

        teacher = User()
        teacher.username = form.cleaned_data['username']
        teacher.first_name = form.cleaned_data['first_name']
        teacher.last_name = form.cleaned_data['last_name']
        teacher.email = form.cleaned_data['email']
        teacher.password = "foobarbaz"
        teacher.save()

        Group.objects.get(name='teachers').user_set.add(teacher)

        return redirect('teachers.index')


class TeacherEditView(CoordinatorView, UpdateView):
    model = User
    form_class = TeacherForm
    pk_url_kwarg = 'id'
    template_name = 'classroom/teacher/edit.html'
    success_url = '/teachers'


class TeacherDeleteView(CoordinatorView, DeleteView):
    model = User
    pk_url_kwarg = 'id'
    template_name = 'classroom/teacher/delete.html'
    success_url = '/teachers'
