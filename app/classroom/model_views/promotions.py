from django.views.generic import UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import Group
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.shortcuts import get_list_or_404, get_object_or_404
from classroom.templatetags.classroom_extras import is_coordinator
from classroom.forms import ProfileForm, StudentForm, PromotionForm
from classroom.templatetags.classroom_extras import is_student, is_coordinator
from classroom.models import Promotion, User, Subject
from classroom.views import CoordinatorView

class PromotionViewStudent(CoordinatorView, TemplateView):
    template_name = "promotion/student.html"

    def test_func(self):
        return is_student(self.request.user)
    
    def get(self, request):
        students = User.objects.filter(promotion=request.user.promotion.id)
        return render(request, self.template_name, {'promotion': request.user.promotion, 'students': students})


class PromotionViewCoordinator(CoordinatorView, TemplateView):
    template_name = "promotion/coordinator.html"

    def test_func(self):
        return is_coordinator(self.request.user)
    
    def get(self, request):
        promotions = Promotion.objects.filter()
        return render(request, self.template_name, {'promotions': promotions})


class PromotionViewCoordinatorDetail(CoordinatorView, TemplateView):
    template_name = "promotion/coordinator_detail.html"

    def test_func(self):
        return is_coordinator(self.request.user)
    
    def get(self, request, id):
        promotion = get_object_or_404(Promotion, id=id)
        students = User.objects.filter(promotion=promotion.id)
        subjects = Subject.objects.filter(promotions=promotion.id)
        return render(request, self.template_name, {'promotion': promotion, 'students': students, 'subjects': subjects})


class PromotionViewCoordinatorAddStudent(CoordinatorView, TemplateView):
    template_name = "promotion/coordinator_add_student.html"

    def test_func(self):
        return is_coordinator(self.request.user)
    
    def get(self, request, id):
        promotion = get_object_or_404(Promotion, id=id)
        students = User.objects.filter(groups__name__in=["students"], promotion__isnull=True)
        return render(request, self.template_name, {'promotion': promotion, 'students': students})

    def post(self, request, id):
        promotion = get_object_or_404(Promotion, id=id)
        for user in [get_object_or_404(User, id=id.split('-')[1]) for id in request.POST if 'user' in id]:
            user.promotion = promotion
            user.save()
        return redirect('promotion.coordinator.detail', id=id)


class PromotionViewCoordinatorRemoveStudent(CoordinatorView, TemplateView):
    def test_func(self):
        return is_coordinator(self.request.user)

    def post(self, request):
        user = get_object_or_404(User, id=request.POST["user_id"])
        id = user.promotion.id
        user.promotion = None
        user.save()
        return redirect('promotion.coordinator.detail', id=id)

class AddPromotionView(CoordinatorView, CreateView):
    model = Promotion
    form_class = PromotionForm
    template_name = 'promotion/create.html'
    success_url = '/promotions'