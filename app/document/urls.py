from django.conf import settings
from django.views.static import serve
from django.contrib.auth.decorators import login_required
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.DocumentIndexView.as_view(), name='document'),
    path('upload/', views.DocumentFormView.as_view(), name='document.upload'),
]
