from django.db import models
from django.utils.http import urlsafe_base64_encode
from classroom.models import User, Subject


def hashed_directory(instance, filename):
    return f"uploads/{urlsafe_base64_encode(bytes(filename, 'utf-8'))}/{filename}"


class Document(models.Model):
    title = models.CharField(max_length=100)
    file = models.FileField(upload_to=hashed_directory)
    uploaded_by = models.ForeignKey(User, on_delete=models.PROTECT)
    subject = models.ForeignKey(Subject, on_delete=models.PROTECT)
    uploaded_at = models.TimeField(auto_now=True)

    def __str__(self):
        return f"{self.title} - {self.file.name.split('/')[-1]}"
