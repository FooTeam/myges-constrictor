from django.forms import ModelForm, FileField, ClearableFileInput
from .models import Document


class DocumentForm(ModelForm):
    file_field = FileField(label="File",
                           widget=ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = Document
        fields = ("title", "subject")
