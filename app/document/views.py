from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import FormView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from classroom.models import Subject, Promotion
from .forms import DocumentForm
from .models import Document


class DocumentIndexView(LoginRequiredMixin, ListView):
    model = Document
    template_name = 'document/index.html'
    context_object_name = 'documents'

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.groups.filter(name='teachers'):
            return queryset.filter(uploaded_by=self.request.user)

        return queryset.filter(subject__in=Subject.objects.filter(
            promotions__in=[self.request.user.promotion]))

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)

        if self.request.user.groups.filter(name='teachers'):
            subjects = Subject.objects.filter(
                teacher=self.request.user)
        elif self.request.user.groups.filter(name='students'):
            subjects = Subject.objects.filter(
                promotions__in=[self.request.user.promotion])

        object_list = {}
        for subject in subjects:
            object_list[subject] = context_data[self.get_context_object_name(
                context_data)].filter(subject=subject)

        context_data[self.get_context_object_name(context_data)] = object_list

        return context_data


class DocumentFormView(LoginRequiredMixin, UserPassesTestMixin, FormView):
    form_class = DocumentForm
    template_name = 'document/upload.html'
    success_url = '/documents/'

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class=form_class)
        files = request.FILES.getlist('file_field')
        if form.is_valid():
            for f in files:
                instance = Document(title=form.data['title'], uploaded_by=request.user, subject=Subject(
                    id=form.data['subject']), file=f)
                instance.save()

            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def test_func(self):
        return self.request.user.groups.filter(name='teachers').exists()
