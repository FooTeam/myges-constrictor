from django.db import models
from classroom.models import Subject
from classroom.models import User

# Create your models here.


class Mark(models.Model):
    value = models.IntegerField()
    subject = models.ForeignKey(Subject, on_delete=models.PROTECT)
    student = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.value} - {self.student.username}"
