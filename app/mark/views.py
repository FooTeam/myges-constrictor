from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Mark
from django.shortcuts import render
from classroom.templatetags.classroom_extras import is_student
from django.views.generic.base import TemplateView


class StudentView(LoginRequiredMixin, UserPassesTestMixin):
    """Class to extends for views reserved to coordinators"""

    def test_func(self):
        return is_student(self.request.user)


class MyMark(StudentView, TemplateView):
    template_name = 'index.html'

    def get(self, request):
        marks = {}
        for mark in Mark.objects.filter(student=request.user):
            subject = mark.subject.name
            marks[subject] = [
                mark] if subject not in marks else marks[subject] + [mark]
        return render(request, 'index.html', {'marks': marks})
