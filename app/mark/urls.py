from django.urls import path
from . import views
from .views import MyMark


urlpatterns = [
    path('', MyMark.as_view(), name='marks.index'),
]
